import { HomePage } from './../pages/home/home';
import { TabPage } from './../pages/tab/tab';
import { Splash } from './../pages/splash/splash';
import { ModalController } from 'ionic-angular';
//import { DatepickerPage } from './../pages/datepicker/datepicker';
import { TodoPage } from './../pages/todo/todo';
import { NotePage } from './../pages/note/note';
import { GoddessPage } from './../pages/goddess/goddess';


//import { HabittrackerPage } from './../pages/habittracker/habittracker';
import { EventModalPage } from './../pages/event-modal/event-modal';
import { Component, ViewChild } from '@angular/core';
import { Platform,Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = TabPage;
  pages: Array<{title:String, component:any}>;
  activePage:any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, modalCtrl: ModalController) {
    
           platform.ready().then(() => {
    
               statusBar.styleDefault();
    
               let splash = modalCtrl.create(Splash);
               splash.present();
    
           });
    this.pages = [
      {title:' Daily Tracker', component:TabPage},
      {title: ' Calender', component:HomePage},
      //{title: 'page oka', component:EventModalPage},
     // {title:' HabitTracker', component:HabittrackerPage},
      //{title:'DatePicker', component:DatepickerPage},
      {title:'To-Dos', component:TodoPage},
      {title:' Note', component:NotePage},
      {title:' My Goddess Time', component:GoddessPage}
  
      
    ];
    
    this.activePage = this.pages[0];

  }
  openPage(page){
    this.nav.setRoot(page.component);
    this.activePage = page;
  }
  checkActive(page){
    return page == this.activePage;
  }
}

