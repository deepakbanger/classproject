import { GrateTPage } from './../pages/grate-t/grate-t';
import { ApmntTPage } from './../pages/apmnt-t/apmnt-t';
import { TodoTPage } from './../pages/todo-t/todo-t';
import { PrioritiesTPage } from './../pages/priorities-t/priorities-t';
import { TabPage } from './../pages/tab/tab';

import { EventModalPage } from './../pages/event-modal/event-modal';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
//import { DatepickerPage } from '../pages/datepicker/datepicker';
//import { HabittrackerPage } from './../pages/habittracker/habittracker';


import { NgCalendarModule } from "ionic2-Calendar";
import { TodoPage } from './../pages/todo/todo';
import { NotePage } from './../pages/note/note';
import { GoddessPage } from './../pages/goddess/goddess';
import { AddNotePage } from './../pages/add-note/add-note';
import { AddItemPage } from './../pages/add-item/add-item';
import { ItemDetailsPage } from '../pages/item-details/item-details';
import { NoteDetailsPage } from '../pages/note-details/note-details';
import { Data } from '../providers/data/data';
import { Datanote } from '../providers/datanote/datanote';
import { Splash } from '../pages/splash/splash';

import { DragulaModule } from 'ng2-dragula';

//import { DatanoteProvider} from '../providers/datanote/datanote';
//import { NotedataProvider } from '../providers/notedata/notedata';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TodoPage,
    NotePage,
    AddNotePage,
    AddItemPage,
    ItemDetailsPage,
    NoteDetailsPage,
    GoddessPage,
    Splash,
    TabPage,
    PrioritiesTPage,
    TodoTPage,
    ApmntTPage,
    GrateTPage
    
  ],
  imports: [
    NgCalendarModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    DragulaModule
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    //HabittrackerPage,
    //DatepickerPage,
    TodoPage,
    NotePage,
    AddNotePage,
    AddItemPage,
    ItemDetailsPage,
    NoteDetailsPage,
    GoddessPage,
    Splash,
    TabPage,
    PrioritiesTPage,
    TodoTPage,
    ApmntTPage,
    GrateTPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Data,
    Datanote
    
    //NotedataProvider
  ]
})
export class AppModule {}
