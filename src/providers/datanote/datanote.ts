
import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
//import { Http } from '@angular/http';
//mport 'rxjs/add/operator/map';


@Injectable()
export class Datanote {

  constructor(public storage: Storage) {
    //console.log('Hello DatanoteProvider Provider');
  }
  getDatanote() {
    return this.storage.get('notes');  
  }
 

  save(datanote){
    let newData = JSON.stringify(datanote);
    this.storage.set('notes', newData);
  }
  remove(datanote)
  {
    let newData = JSON.stringify(datanote);
    this.storage.set('notes',newData);
  }
}

