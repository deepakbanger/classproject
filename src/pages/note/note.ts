import { Component } from '@angular/core';
import { ModalController, IonicPage, NavController, NavParams } from 'ionic-angular';

import { AddNotePage } from '../add-note/add-note'
import { NoteDetailsPage } from '../note-details/note-details';
import {Datanote } from '../../providers/datanote/datanote';
import { DragulaService } from 'ng2-dragula/ng2-dragula';
//import { Data } from '../../providers/data/data';
/**
 * Generated class for the NotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-note',
  templateUrl: 'note.html',
})

export class NotePage {
  alertCtrl: any;
    notes: string[] = [];
    note: string;
    public notess =[];
    q1 = [];
    q2 = [];
    //private alertCtrl: AlertController;
 
    //constructor(public navCtrl: NavController) {}
    constructor(public navCtrl: NavController, public modalCtrl: ModalController,private dragulaService: DragulaService,public dataService: Datanote) {
        
          this.dataService.getDatanote().then((notes) => 
          {
        
              if (notes) {
               this.notes = JSON.parse(notes);
            }
        
           });
           
        
          }    
         
        
          ionViewDidLoad() {
        
          }
          addNote() {
            
                let addModal = this.modalCtrl.create(AddNotePage);
            
                addModal.onDidDismiss((note) => {
            
                  if (note) {
                    this.saveNote(note);
                  }
            
                });
            
                addModal.present();
            
              }
            
              saveNote(note) {
                this.notes.push(note);
              this.dataService.save(this.notes);
              }
            
              viewNote(note) {
                this.navCtrl.push(NoteDetailsPage, {
                  note: note
                });
              }
              removeNote(note){
                for(var i = 0; i < this.notes.length; i++) {
                  if(this.notes[i] == note){
                    this.notes.splice(i, 1);
                    this.dataService.remove(this.notes);
                  }
                }
              }
              
            }
            
            
           