import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GrateTPage } from './grate-t';

@NgModule({
  declarations: [
    GrateTPage,
  ],
  imports: [
    IonicPageModule.forChild(GrateTPage),
  ],
})
export class GrateTPageModule {}
