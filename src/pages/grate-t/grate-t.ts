import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GrateTPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-grate-t',
  templateUrl: 'grate-t.html',
})
export class GrateTPage {
  grates: string[] = [];
  grate: string;
  addCounter:number = 3;
  deleteCounter: number = 1;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  add() {
    this.grates.push(this.grate);
    this.grate = "";

}

delete(item) {
    var index = this.grates.indexOf(item, 0);
    if (index > -1) {
        this.grates.splice(index, 1);
    }
}
}
