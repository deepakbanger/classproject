import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ApmntTPage } from './apmnt-t';

@NgModule({
  declarations: [
    ApmntTPage,
  ],
  imports: [
    IonicPageModule.forChild(ApmntTPage),
  ],
})
export class ApmntTPageModule {}
