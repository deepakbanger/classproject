import { TodoTPage } from './../todo-t/todo-t';

import { Component } from '@angular/core';

import { ModalController, NavController, AlertController, reorderArray } from 'ionic-angular';
import { AddItemPage } from '../add-item/add-item'
import { ItemDetailsPage } from '../item-details/item-details';
import { Data } from '../../providers/data/data';

@Component({
    selector: 'page-todo',
    templateUrl: 'todo.html'
})
export class TodoPage {
    todos: string[] = [];
    todo: string;
    public items =[];
    //private alertCtrl: AlertController;
 
    //constructor(public navCtrl: NavController) {}
    constructor(public navCtrl: NavController, public modalCtrl: ModalController, public dataService: Data) {
      for (let x = 0; x < 5; x++) {
        this.items.push(x);
      }
    
           this.dataService.getData().then((todos) => 
           {
        
              if (todos) {
                this.items = JSON.parse(todos);
              }
        
            });
        
          }
        
          ionViewDidLoad() {
        
          }
          addItem() {
            
                let addModal = this.modalCtrl.create(AddItemPage);
            
                addModal.onDidDismiss((item) => {
            
                  if (item) {
                    this.saveItem(item);
                  }
            
                });
            
                addModal.present();
            
              }
            
              saveItem(item) {
                this.items.push(item);
                this.dataService.save(this.items);
              }
    
            
            removeItem(item){
                 for(var i = 0; i < this.items.length; i++) {
                   if(this.items[i] == item){
                     this.items.splice(i, 1);
                     this.dataService.remove(this.items);
                   }
                 }
               }
               reorderItems(item) {
                this.items = reorderArray(this.items, item);
              }

              moveTodos(item){
                this.navCtrl.push(TodoTPage,{"move":item.title});
              }
              }
          
            
            /*  editItem(item){
                for(var i = 0; i < this.items.length; i++) {
                  if(this.items[i] == item){
                this.navCtrl.push(AddItemPage,this.items[i]);
              }
          
            }
              } */
        


            
            
            //
/**add() {
        this.todos.push(this.todo);
        this.todo = "";
    }
 
    delete(item) {
        var index = this.todos.indexOf(item, 0);
        if (index > -1) {
            this.todos.splice(index, 1);
        }
    }
}


  delete(todo) {
                var index = this.todos.indexOf(todo, 0);
                if (index > -1) {
                    this.todos.splice(index, 1);
                }
            }
        }**/