import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GoddessPage } from './goddess';

@NgModule({
  declarations: [
    GoddessPage,
  ],
  imports: [
    IonicPageModule.forChild(GoddessPage),
  ],
})
export class GoddessPageModule {}
