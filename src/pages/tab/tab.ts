import { PrioritiesTPage } from './../priorities-t/priorities-t';
import { TodoTPage } from './../todo-t/todo-t';
import { GrateTPage } from './../grate-t/grate-t';
import { ApmntTPage } from './../apmnt-t/apmnt-t';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tab',
  templateUrl: 'tab.html',
})
export class TabPage {

ApmntTPage = ApmntTPage;
GrateTPage = GrateTPage;
TodoTPage = TodoTPage;
PrioritiesTPage = PrioritiesTPage;

}
