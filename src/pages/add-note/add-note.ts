import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';

/**
 * Generated class for the AddNotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-add-note',
  templateUrl: 'add-note.html',
})
export class AddNotePage {
  title;
  description;
  constructor(public navCtrl: NavController, public view: ViewController) {
    
      }
  saveNote() {
    
        let newNote = {
          title: this.title,
          description: this.description
        };
    
        this.view.dismiss(newNote);
    
      }
    
      close() {
        this.view.dismiss();
      }
    
    }
