import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
 
@Component({
  selector: 'page-splash',
  templateUrl: 'splash.html',
})
export class Splash {
 
  constructor(public viewCtrl: ViewController, public splashScreen: SplashScreen) {
 
  }
 
  ionViewDidEnter() {
 
    this.splashScreen.hide();
 
    setTimeout(() => {
      this.viewCtrl.dismiss();
    }, 4000);

    var d = new Date();
    var x = document.getElementById("greetings");
    var h =d.getHours();
    if (h<12)
    x.innerHTML = "good morning!";
    else
    x.innerHTML = "good afternoon!";
 
  }
 
}