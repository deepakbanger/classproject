import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrioritiesTPage } from './priorities-t';

@NgModule({
  declarations: [
    PrioritiesTPage,
  ],
  imports: [
    IonicPageModule.forChild(PrioritiesTPage),
  ],
})
export class PrioritiesTPageModule {}
