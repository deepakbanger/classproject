import { HomePage } from './../home/home';

import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController,Nav } from 'ionic-angular';
import * as moment from 'moment';
 
@IonicPage()
@Component({
  selector: 'page-event-modal',
  templateUrl: 'event-modal.html',
})
export class EventModalPage {
  @ViewChild(Nav) nav: Nav;
  
  event = { startTime: new Date().toISOString(), endTime: new Date().toISOString(), allDay: false };
  minDate = new Date().toISOString();
 
  constructor(public navCtrl: NavController, private navParams: NavParams, public viewCtrl: ViewController) {
    let preselectedDate = moment(this.navParams.get('selectedDay')).format();
    this.event.startTime = preselectedDate;
    this.event.endTime = preselectedDate;
  }
 
  cancel() {
    this.nav.setRoot(HomePage);
    //this.nav.setRoot(HomePage);
  }
 
  save() {
    this.viewCtrl.dismiss(this.event);
  }
 
}