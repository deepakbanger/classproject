import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TodoTPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-todo-t',
  templateUrl: 'todo-t.html',
})
export class TodoTPage {
 title;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
   
    this.title = this.navParams.get('move');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TodoTPage');
  }

}
