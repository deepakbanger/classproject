import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TodoTPage } from './todo-t';

@NgModule({
  declarations: [
    TodoTPage,
  ],
  imports: [
    IonicPageModule.forChild(TodoTPage),
  ],
})
export class TodoTPageModule {}
