webpackJsonp([12],{

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GrateTPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the GrateTPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GrateTPage = (function () {
    function GrateTPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.grates = [];
        this.addCounter = 3;
        this.deleteCounter = 1;
    }
    GrateTPage.prototype.add = function () {
        this.grates.push(this.grate);
        this.grate = "";
    };
    GrateTPage.prototype.delete = function (item) {
        var index = this.grates.indexOf(item, 0);
        if (index > -1) {
            this.grates.splice(index, 1);
        }
    };
    return GrateTPage;
}());
GrateTPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-grate-t',template:/*ion-inline-start:"/Users/deepakbanger/Desktop/gofro/src/pages/grate-t/grate-t.html"*/'<ion-content>\n    <ion-item>\n        <ion-input [(ngModel)]="grate" placeholder="Add today\'s gratitude here" type="text"></ion-input>\n    </ion-item>\n\n    <button ion-button full (click)="add()">ADD</button>\n\n    <ion-list>\n        <ion-item-sliding *ngFor="let t of grates">\n            <ion-item>\n                {{t}}\n            </ion-item>\n\n            <ion-item-options side="right">\n                <button ion-button color="danger" (click)="delete(t)">\n                    <ion-icon name="trash"></ion-icon> Delete\n                </button>\n            </ion-item-options>\n        </ion-item-sliding>\n    </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/deepakbanger/Desktop/gofro/src/pages/grate-t/grate-t.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
], GrateTPage);

//# sourceMappingURL=grate-t.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrioritiesTPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the PrioritiesTPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PrioritiesTPage = (function () {
    function PrioritiesTPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    PrioritiesTPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PrioritiesTPage');
    };
    return PrioritiesTPage;
}());
PrioritiesTPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-priorities-t',template:/*ion-inline-start:"/Users/deepakbanger/Desktop/gofro/src/pages/priorities-t/priorities-t.html"*/'<ion-content padding>\n\n</ion-content>'/*ion-inline-end:"/Users/deepakbanger/Desktop/gofro/src/pages/priorities-t/priorities-t.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
], PrioritiesTPage);

//# sourceMappingURL=priorities-t.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApmntTPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ApmntTPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ApmntTPage = (function () {
    function ApmntTPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ApmntTPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ApmntTPage');
    };
    return ApmntTPage;
}());
ApmntTPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-apmnt-t',template:/*ion-inline-start:"/Users/deepakbanger/Desktop/gofro/src/pages/apmnt-t/apmnt-t.html"*/'<ion-content padding>\n\n</ion-content>'/*ion-inline-end:"/Users/deepakbanger/Desktop/gofro/src/pages/apmnt-t/apmnt-t.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
], ApmntTPage);

//# sourceMappingURL=apmnt-t.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__priorities_t_priorities_t__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__todo_t_todo_t__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__grate_t_grate_t__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__apmnt_t_apmnt_t__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






/**
 * Generated class for the TabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TabPage = (function () {
    function TabPage() {
        this.ApmntTPage = __WEBPACK_IMPORTED_MODULE_3__apmnt_t_apmnt_t__["a" /* ApmntTPage */];
        this.GrateTPage = __WEBPACK_IMPORTED_MODULE_2__grate_t_grate_t__["a" /* GrateTPage */];
        this.TodoTPage = __WEBPACK_IMPORTED_MODULE_1__todo_t_todo_t__["a" /* TodoTPage */];
        this.PrioritiesTPage = __WEBPACK_IMPORTED_MODULE_0__priorities_t_priorities_t__["a" /* PrioritiesTPage */];
    }
    return TabPage;
}());
TabPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["Component"])({
        selector: 'page-tab',template:/*ion-inline-start:"/Users/deepakbanger/Desktop/gofro/src/pages/tab/tab.html"*/'<ion-header>\n    <ion-navbar color="primary">\n\n        <ion-buttons start>\n            <button ion-button icon-only menuToggle>\n                   <ion-icon name="menu"></ion-icon>\n                 </button>\n        </ion-buttons>\n        <ion-title>\n            Daily Tracker\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n<ion-content>\n    <ion-tabs>\n        <ion-tab [root]="PrioritiesTPage" tabTitle="Priorities"></ion-tab>\n        <ion-tab [root]="ApmntTPage" tabTitle="Appointment"></ion-tab>\n        <ion-tab [root]="GrateTPage" tabTitle="Gratitude"></ion-tab>\n        <ion-tab [root]="TodoTPage" tabTitle="To-Dos"></ion-tab>\n    </ion-tabs>\n</ion-content>'/*ion-inline-end:"/Users/deepakbanger/Desktop/gofro/src/pages/tab/tab.html"*/,
    })
], TabPage);

//# sourceMappingURL=tab.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddItemPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AddItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AddItemPage = (function () {
    function AddItemPage(navCtrl, view) {
        this.navCtrl = navCtrl;
        this.view = view;
    }
    AddItemPage.prototype.saveItem = function () {
        var newItem = {
            title: this.title,
            date: this.date,
            time: this.time
        };
        this.view.dismiss(newItem);
    };
    AddItemPage.prototype.close = function () {
        this.view.dismiss();
    };
    return AddItemPage;
}());
AddItemPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-add-item',template:/*ion-inline-start:"/Users/deepakbanger/Desktop/gofro/src/pages/add-item/add-item.html"*/'<!--\n  Generated template for the AddItemPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-toolbar color="primary">\n        <ion-title>\n            Add To-do\n        </ion-title>\n        <ion-buttons end>\n            <button ion-button icon-only (click)="close()"><ion-icon name="close"></ion-icon></button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <ion-list>\n        <ion-item>\n            <ion-input type="text" [(ngModel)]="title" placeholder="Add todo\'s here"></ion-input> <br>\n        </ion-item>\n        <ion-item>\n            <ion-label stacked color="primary">Date</ion-label>\n            <ion-input type="date" placeholder="Enter Date" required="required" [(ngModel)]="date"></ion-input>\n        </ion-item>\n\n        <ion-item>\n            <ion-label stacked color="primary">Time</ion-label>\n            <ion-input type="time" placeholder="Enter Time" required="required" [(ngModel)]="time"></ion-input>\n        </ion-item>\n        <button full ion-button color="primary" (click)="saveItem()">Save</button>\n    </ion-list>\n\n</ion-content>'/*ion-inline-end:"/Users/deepakbanger/Desktop/gofro/src/pages/add-item/add-item.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ViewController */]])
], AddItemPage);

//ionViewDidLoad() {
//console.log('ionViewDidLoad AddItemPage');
//}}
//# sourceMappingURL=add-item.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddNotePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AddNotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AddNotePage = (function () {
    function AddNotePage(navCtrl, view) {
        this.navCtrl = navCtrl;
        this.view = view;
    }
    AddNotePage.prototype.saveNote = function () {
        var newNote = {
            title: this.title,
            description: this.description
        };
        this.view.dismiss(newNote);
    };
    AddNotePage.prototype.close = function () {
        this.view.dismiss();
    };
    return AddNotePage;
}());
AddNotePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-add-note',template:/*ion-inline-start:"/Users/deepakbanger/Desktop/gofro/src/pages/add-note/add-note.html"*/'<!--\n  Generated template for the AddNotePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-toolbar color="primary">\n    <ion-title>\n      Add Note\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="close()"><ion-icon name="close"></ion-icon></button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n\n    <ion-item>\n      <ion-label floating>Title</ion-label>\n      <ion-input type="text" [(ngModel)]="title"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label floating>Description</ion-label>\n      <ion-input type="text" [(ngModel)]="description"></ion-input>\n    </ion-item>\n\n  </ion-list>\n\n  <button full ion-button color="primary" (click)="saveNote()">Save</button>\n\n</ion-content>\n'/*ion-inline-end:"/Users/deepakbanger/Desktop/gofro/src/pages/add-note/add-note.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ViewController */]])
], AddNotePage);

//# sourceMappingURL=add-note.js.map

/***/ }),

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NoteDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the NoteDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NoteDetailsPage = (function () {
    function NoteDetailsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    NoteDetailsPage.prototype.ionViewDidLoad = function () {
        this.title = this.navParams.get('note').title;
        this.description = this.navParams.get('note').description;
        //console.log('ionViewDidLoad NoteDetailsPage');
    };
    return NoteDetailsPage;
}());
NoteDetailsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-note-details',template:/*ion-inline-start:"/Users/deepakbanger/Desktop/gofro/src/pages/note-details/note-details.html"*/'<!--\n  Generated template for the NoteDetailsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar color="primary">\n\n        <ion-title>\n            <ion-input type="text" [(ngModel)]="title"></ion-input>\n        </ion-title>\n\n\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <ion-card>\n        <ion-textarea rows="10" [(ngModel)]="description"></ion-textarea>\n        <!--  <ion-input type="text" [(ngModel)]="description"></ion-input> -->\n    </ion-card>\n\n\n</ion-content>'/*ion-inline-end:"/Users/deepakbanger/Desktop/gofro/src/pages/note-details/note-details.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
], NoteDetailsPage);

//# sourceMappingURL=note-details.js.map

/***/ }),

/***/ 113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_note_add_note__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__note_details_note_details__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_datanote_datanote__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_dragula_ng2_dragula__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_dragula_ng2_dragula___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng2_dragula_ng2_dragula__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






//import { Data } from '../../providers/data/data';
/**
 * Generated class for the NotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NotePage = (function () {
    //private alertCtrl: AlertController;
    //constructor(public navCtrl: NavController) {}
    function NotePage(navCtrl, modalCtrl, dragulaService, dataService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.dragulaService = dragulaService;
        this.dataService = dataService;
        this.notes = [];
        this.notess = [];
        this.q1 = [];
        this.q2 = [];
        this.dataService.getDatanote().then(function (notes) {
            if (notes) {
                _this.notes = JSON.parse(notes);
            }
        });
    }
    NotePage.prototype.ionViewDidLoad = function () {
    };
    NotePage.prototype.addNote = function () {
        var _this = this;
        var addModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__add_note_add_note__["a" /* AddNotePage */]);
        addModal.onDidDismiss(function (note) {
            if (note) {
                _this.saveNote(note);
            }
        });
        addModal.present();
    };
    NotePage.prototype.saveNote = function (note) {
        this.notes.push(note);
        this.dataService.save(this.notes);
    };
    NotePage.prototype.viewNote = function (note) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__note_details_note_details__["a" /* NoteDetailsPage */], {
            note: note
        });
    };
    NotePage.prototype.removeNote = function (note) {
        for (var i = 0; i < this.notes.length; i++) {
            if (this.notes[i] == note) {
                this.notes.splice(i, 1);
                this.dataService.remove(this.notes);
            }
        }
    };
    return NotePage;
}());
NotePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-note',template:/*ion-inline-start:"/Users/deepakbanger/Desktop/gofro/src/pages/note/note.html"*/'<ion-header>\n    <ion-navbar color="primary">\n\n        <ion-buttons start>\n            <button ion-button icon-only menuToggle>\n                <ion-icon name="menu"></ion-icon>\n              </button>\n        </ion-buttons>\n\n        <ion-title>\n            Notes!\n        </ion-title>\n        <ion-buttons end>\n            <button ion-button icon-only (click)="addNote()"><ion-icon name="add-circle"></ion-icon></button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <!--- <ion-list>\n      \n    <ion-item *ngFor="let note of notes" (click)="viewNote(note)">{{note.title}}</ion-item>\n  </ion-list>-->\n\n\n    <ion-card *ngFor="let note of notes" (click)="viewNote(note)">\n\n        <ion-item>\n            <h1>\n                <ion-avatar item-start>\n\n                    {{note.title}}\n\n                </ion-avatar>\n            </h1>\n            <button ion-button color="light" (click)="removeNote(note)"><ion-icon name="trash" style="float: right;"></ion-icon> &nbsp; Delete</button>\n        </ion-item>\n    </ion-card>\n</ion-content>'/*ion-inline-end:"/Users/deepakbanger/Desktop/gofro/src/pages/note/note.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */], __WEBPACK_IMPORTED_MODULE_5_ng2_dragula_ng2_dragula__["DragulaService"], __WEBPACK_IMPORTED_MODULE_4__providers_datanote_datanote__["a" /* Datanote */]])
], NotePage);

//# sourceMappingURL=note.js.map

/***/ }),

/***/ 114:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GoddessPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the GoddessPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GoddessPage = (function () {
    function GoddessPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    GoddessPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad GoddessPage');
    };
    return GoddessPage;
}());
GoddessPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-goddess',template:/*ion-inline-start:"/Users/deepakbanger/Desktop/gofro/src/pages/goddess/goddess.html"*/'<!--\n  Generated template for the GoddessPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="primary">\n      \n<ion-buttons start>\n       <button ion-button icon-only menuToggle>\n                   <ion-icon name="menu"></ion-icon>\n                 </button>\n   </ion-buttons>\n    <ion-title>\n    My Goddess Time!\n  \n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n  <ion-content > \n\n    \n    <ion-list>\n        <ion-item>\n          <ion-label>\n            reading a book at my favorite coffee shop</ion-label>\n          <ion-checkbox color="dark" checked="true"></ion-checkbox>\n        </ion-item>\n        <ion-item>\n          <ion-label>going to the spa with my favorite book or with a girlfriend</ion-label>\n          <ion-checkbox color="danger" checked="true"></ion-checkbox>\n        </ion-item>\n        <ion-item>\n          <ion-label>taking a hot bath with candles and bath salts & wine</ion-label>\n          <ion-checkbox></ion-checkbox>\n        </ion-item>\n        <ion-item>\n            <ion-label>taking a hot bath with candles and bath salts & wine</ion-label>\n            <ion-checkbox></ion-checkbox>\n          </ion-item>\n         <ion-item>\n              <ion-label>buying a beautiful journal & journaling in the park</ion-label>\n              <ion-checkbox></ion-checkbox>\n            </ion-item>\n           <ion-item>\n                <ion-label>curling up in bed well before I go to sleep, reading, drinking tea</ion-label>\n                <ion-checkbox></ion-checkbox>\n              </ion-item>\n             <ion-item>\n                  <ion-label>taking an art, dancing or music class alone or with a girlfriend</ion-label>\n                  <ion-checkbox></ion-checkbox>\n                </ion-item>\n                <ion-item>\n                    <ion-label>creating & honoring my morning routine</ion-label>\n                    <ion-checkbox></ion-checkbox>\n                  </ion-item>\n                  <ion-item>\n                      <ion-label>going for a walk around the neighborhood</ion-label>\n                      <ion-checkbox></ion-checkbox>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>sitting on a park bench and people watching</ion-label>\n                        <ion-checkbox></ion-checkbox>\n                      </ion-item>\n                     <ion-item>\n                          <ion-label>listening to spiritual audios or videos that inspire me</ion-label>\n                          <ion-checkbox></ion-checkbox>\n                        </ion-item>\n                        <ion-item>\n                            <ion-label>create my vision board & look at it daily, visualizing & feeling</ion-label>\n                            <ion-checkbox></ion-checkbox>\n                          </ion-item>\n                         <ion-item>\n                              <ion-label>do an online guided meditation</ion-label>\n                              <ion-checkbox></ion-checkbox>\n                            </ion-item>\n                            <ion-item>\n                                <ion-label>participate in a global online meditation</ion-label>\n                                <ion-checkbox></ion-checkbox>\n                              </ion-item>\n                              <ion-item>\n                                  <ion-label>write a card to someone and mail it</ion-label>\n                                  <ion-checkbox></ion-checkbox>\n                                </ion-item>\n                                <ion-item>\n                                    <ion-label>set an intention while I relax & color sacred geometry</ion-label>\n                                    <ion-checkbox></ion-checkbox>\n                                  </ion-item>\n                                  <ion-item>\n                                      <ion-label>offer to pay for a strangers coffee</ion-label>\n                                      <ion-checkbox></ion-checkbox>\n                                    </ion-item>\n                                                                                                                                                                     \n                                  \n          \n        </ion-list>\n    </ion-content>'/*ion-inline-end:"/Users/deepakbanger/Desktop/gofro/src/pages/goddess/goddess.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
], GoddessPage);

//# sourceMappingURL=goddess.js.map

/***/ }),

/***/ 115:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomePage = (function () {
    function HomePage(navCtrl, modalCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.eventSource = [];
        this.selectedDay = new Date();
        this.calendar = {
            mode: 'month',
            currentDate: new Date()
        };
    }
    HomePage.prototype.addEvent = function () {
        var _this = this;
        var modal = this.modalCtrl.create('EventModalPage', { selectedDay: this.selectedDay });
        modal.present();
        modal.onDidDismiss(function (data) {
            if (data) {
                var eventData = data;
                eventData.startTime = new Date(data.startTime);
                eventData.endTime = new Date(data.endTime);
                var events_1 = _this.eventSource;
                events_1.push(eventData);
                _this.eventSource = [];
                setTimeout(function () {
                    _this.eventSource = events_1;
                });
            }
        });
    };
    HomePage.prototype.onViewTitleChanged = function (title) {
        this.viewTitle = title;
    };
    HomePage.prototype.onEventSelected = function (event) {
        var start = __WEBPACK_IMPORTED_MODULE_2_moment__(event.startTime).format('LLLL');
        var end = __WEBPACK_IMPORTED_MODULE_2_moment__(event.endTime).format('LLLL');
        var alert = this.alertCtrl.create({
            title: '' + event.title,
            subTitle: 'From: ' + start + '<br>To: ' + end,
            buttons: ['OK']
        });
        alert.present();
    };
    HomePage.prototype.onTimeSelected = function (ev) {
        this.selectedDay = ev.selectedTime;
    };
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-home',template:/*ion-inline-start:"/Users/deepakbanger/Desktop/gofro/src/pages/home/home.html"*/'<!-- helloo guys lets do it -->\n\n<ion-header>\n    <ion-navbar color="primary">\n\n        <ion-buttons start>\n            <button ion-button icon-only menuToggle>\n                          <ion-icon name="menu"></ion-icon>\n                        </button>\n        </ion-buttons>\n\n        <ion-title>\n            {{ viewTitle }}\n        </ion-title>\n        <ion-buttons end>\n            <button ion-button icon-only (click)="addEvent()">\n        <ion-icon name="add"></ion-icon>\n      </button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n<ion-content>\n    <calendar [eventSource]="eventSource" [calendarMode]="calendar.mode" [currentDate]="calendar.currentDate" (onEventSelected)="onEventSelected($event)" (onTitleChanged)="onViewTitleChanged($event)" (onTimeSelected)="onTimeSelected($event)" step="30" class="calender"></calendar>\n</ion-content>'/*ion-inline-end:"/Users/deepakbanger/Desktop/gofro/src/pages/home/home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 123:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 123;

/***/ }),

/***/ 164:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/add-item/add-item.module": [
		433,
		11
	],
	"../pages/add-note/add-note.module": [
		435,
		10
	],
	"../pages/apmnt-t/apmnt-t.module": [
		431,
		9
	],
	"../pages/event-modal/event-modal.module": [
		439,
		0
	],
	"../pages/goddess/goddess.module": [
		438,
		8
	],
	"../pages/grate-t/grate-t.module": [
		430,
		7
	],
	"../pages/item-details/item-details.module": [
		434,
		6
	],
	"../pages/note-details/note-details.module": [
		436,
		5
	],
	"../pages/note/note.module": [
		437,
		4
	],
	"../pages/priorities-t/priorities-t.module": [
		428,
		3
	],
	"../pages/tab/tab.module": [
		432,
		2
	],
	"../pages/todo-t/todo-t.module": [
		429,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 164;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 165:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Datanote; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_storage__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { Http } from '@angular/http';
//mport 'rxjs/add/operator/map';
var Datanote = (function () {
    function Datanote(storage) {
        this.storage = storage;
        //console.log('Hello DatanoteProvider Provider');
    }
    Datanote.prototype.getDatanote = function () {
        return this.storage.get('notes');
    };
    Datanote.prototype.save = function (datanote) {
        var newData = JSON.stringify(datanote);
        this.storage.set('notes', newData);
    };
    Datanote.prototype.remove = function (datanote) {
        var newData = JSON.stringify(datanote);
        this.storage.set('notes', newData);
    };
    return Datanote;
}());
Datanote = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__ionic_storage__["b" /* Storage */]])
], Datanote);

//# sourceMappingURL=datanote.js.map

/***/ }),

/***/ 325:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Splash; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_splash_screen__ = __webpack_require__(104);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Splash = (function () {
    function Splash(viewCtrl, splashScreen) {
        this.viewCtrl = viewCtrl;
        this.splashScreen = splashScreen;
    }
    Splash.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.splashScreen.hide();
        setTimeout(function () {
            _this.viewCtrl.dismiss();
        }, 4000);
        var d = new Date();
        var x = document.getElementById("greetings");
        var h = d.getHours();
        if (h < 12)
            x.innerHTML = "good morning!";
        else
            x.innerHTML = "good afternoon!";
    };
    return Splash;
}());
Splash = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-splash',template:/*ion-inline-start:"/Users/deepakbanger/Desktop/gofro/src/pages/splash/splash.html"*/'<ion-content>\n\n    <svg id="bars" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 63.15 224.35">\n         <defs>\n             <style>.cls-1{fill:#dd238c;}.cls-2{fill:#ef4328;}.cls-3{fill:#7dd0df;}.cls-4{fill:#febf12;}.cls-5{fill:#282828;}</style>\n         </defs>\n         <title>Good Morning </title>\n         <rect class="cls-1" x="27.22" width="20.06" height="163.78"/>\n         <! <rect class="cls-2" y="4" width="20.06" height="163.78"/>\n         <circle class="cls-2" cx="50" cy="50" r="40" width="20.06" height="150" />\n         <rect class="cls-3" x="13.9" y="13.1" width="20.06" height="163.78"/>\n         <rect class="cls-4" x="43.1" y="7.45" width="20.06" height="163.78"/>\n         <! <path class="cls-5" d="M243.5,323a12,12,0,0,1-.5,3.43,8.88,8.88,0,0,1-1.63,3.1,8.24,8.24,0,0,1-3,2.26,10.8,10.8,0,0,1-4.58.86,9.63,9.63,0,0,1-6-1.82,8.48,8.48,0,0,1-3.07-5.47l4-.82a5.64,5.64,0,0,0,1.66,3.19,4.86,4.86,0,0,0,3.43,1.18,5.71,5.71,0,0,0,2.83-.62,4.53,4.53,0,0,0,1.7-1.63,7,7,0,0,0,.84-2.33,15.15,15.15,0,0,0,.24-2.71V297.82h4V323Z" transform="translate(-224.04 -108.31)"/>\n         <! <path class="cls-5" d="M252,297.82h6l11.52,26.64h0.1l11.62-26.64H287v34h-4V303.29h-0.1L270.72,331.8h-2.45l-12.19-28.51H256V331.8h-4v-34Z" transform="translate(-224.04 -108.31)"/>\n     </svg>\n    <br>\n\n    <h1 id="greetings">\n\n    </h1>\n    <!  <img src="../../assets/background/backg.png" class="cls-5">\n\n</ion-content>'/*ion-inline-end:"/Users/deepakbanger/Desktop/gofro/src/pages/splash/splash.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_splash_screen__["a" /* SplashScreen */]])
], Splash);

//# sourceMappingURL=splash.js.map

/***/ }),

/***/ 326:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TodoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__todo_t_todo_t__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__add_item_add_item__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_data_data__ = __webpack_require__(327);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TodoPage = (function () {
    //private alertCtrl: AlertController;
    //constructor(public navCtrl: NavController) {}
    function TodoPage(navCtrl, modalCtrl, dataService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.dataService = dataService;
        this.todos = [];
        this.items = [];
        for (var x = 0; x < 5; x++) {
            this.items.push(x);
        }
        this.dataService.getData().then(function (todos) {
            if (todos) {
                _this.items = JSON.parse(todos);
            }
        });
    }
    TodoPage.prototype.ionViewDidLoad = function () {
    };
    TodoPage.prototype.addItem = function () {
        var _this = this;
        var addModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__add_item_add_item__["a" /* AddItemPage */]);
        addModal.onDidDismiss(function (item) {
            if (item) {
                _this.saveItem(item);
            }
        });
        addModal.present();
    };
    TodoPage.prototype.saveItem = function (item) {
        this.items.push(item);
        this.dataService.save(this.items);
    };
    TodoPage.prototype.removeItem = function (item) {
        for (var i = 0; i < this.items.length; i++) {
            if (this.items[i] == item) {
                this.items.splice(i, 1);
                this.dataService.remove(this.items);
            }
        }
    };
    TodoPage.prototype.reorderItems = function (item) {
        this.items = Object(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* reorderArray */])(this.items, item);
    };
    TodoPage.prototype.moveTodos = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_0__todo_t_todo_t__["a" /* TodoTPage */], { "move": item.title });
    };
    return TodoPage;
}());
TodoPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
        selector: 'page-todo',template:/*ion-inline-start:"/Users/deepakbanger/Desktop/gofro/src/pages/todo/todo.html"*/'<ion-header>\n    <ion-navbar color="primary">\n\n        <ion-buttons start>\n            <button ion-button icon-only menuToggle>\n                     <ion-icon name="menu"></ion-icon>\n                   </button>\n        </ion-buttons>\n        <ion-title>\n            Todos!\n        </ion-title>\n        <ion-buttons end>\n            <button ion-button icon-only (click)="addItem()"><ion-icon name="add-circle"></ion-icon></button>\n        </ion-buttons>\n\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n    <ion-list>\n        <ion-item-sliding *ngFor="let item of items" reorder="true" (ionItemReorder)="reorderItems($event)">\n\n            <ion-item>\n                <h1>{{item.title}}</h1><br>\n                <span style="color:pink;">{{item.date}}<br>{{item.time}}</span>\n            </ion-item>\n\n            <ion-item-options side="right">\n                <button ion-button color="danger" (click)="removeItem(item)"><ion-icon name="trash"></ion-icon>Delete</button>\n                <button ion-button color="light" (click)="moveTodos(item)">\n                    <ion-icon name="move"></ion-icon>\n                      Move to<br>today\'s<br>Todos\n                    </button>\n            </ion-item-options>\n\n        </ion-item-sliding>\n    </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/deepakbanger/Desktop/gofro/src/pages/todo/todo.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */], __WEBPACK_IMPORTED_MODULE_4__providers_data_data__["a" /* Data */]])
], TodoPage);

/*  editItem(item){
    for(var i = 0; i < this.items.length; i++) {
      if(this.items[i] == item){
    this.navCtrl.push(AddItemPage,this.items[i]);
  }

}
  } */
//
/**add() {
        this.todos.push(this.todo);
        this.todo = "";
    }
 
    delete(item) {
        var index = this.todos.indexOf(item, 0);
        if (index > -1) {
            this.todos.splice(index, 1);
        }
    }
}


  delete(todo) {
                var index = this.todos.indexOf(todo, 0);
                if (index > -1) {
                    this.todos.splice(index, 1);
                }
            }
        }**/ 
//# sourceMappingURL=todo.js.map

/***/ }),

/***/ 327:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Data; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_storage__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { Http } from '@angular/http';
var Data = (function () {
    function Data(storage) {
        this.storage = storage;
    }
    Data.prototype.getData = function () {
        return this.storage.get('todos');
    };
    Data.prototype.save = function (data) {
        var newData = JSON.stringify(data);
        this.storage.set('todos', newData);
    };
    Data.prototype.remove = function (data) {
        var newData = JSON.stringify(data);
        this.storage.set('todos', newData);
    };
    return Data;
}());
Data = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__ionic_storage__["b" /* Storage */]])
], Data);

//# sourceMappingURL=data.js.map

/***/ }),

/***/ 328:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ItemDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ItemDetailsPage = (function () {
    function ItemDetailsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ItemDetailsPage.prototype.ionViewDidLoad = function () {
        this.title = this.navParams.get('item').title;
        this.description = this.navParams.get('item').description;
        //console.log('ionViewDidLoad ItemDetailsPage');
    };
    return ItemDetailsPage;
}());
ItemDetailsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-item-details',template:/*ion-inline-start:"/Users/deepakbanger/Desktop/gofro/src/pages/item-details/item-details.html"*/'<ion-header>\n    <ion-navbar color="primary">\n\n        <ion-title>\n            <ion-input type="text" [(ngModel)]="title"></ion-input>\n        </ion-title>\n\n\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <ion-card>\n        <!--<ion-card-content>\n      {{description}}\n    </ion-card-content>\n    <ion-input type="text" [(ngModel)]="description"></ion-input>-->\n    </ion-card>\n\n</ion-content>'/*ion-inline-end:"/Users/deepakbanger/Desktop/gofro/src/pages/item-details/item-details.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
], ItemDetailsPage);

//# sourceMappingURL=item-details.js.map

/***/ }),

/***/ 329:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(330);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(348);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 348:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_grate_t_grate_t__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_apmnt_t_apmnt_t__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_todo_t_todo_t__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_priorities_t_priorities_t__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tab_tab__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_storage__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_status_bar__ = __webpack_require__(324);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__app_component__ = __webpack_require__(416);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_home_home__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_ionic2_Calendar__ = __webpack_require__(417);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_todo_todo__ = __webpack_require__(326);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_note_note__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_goddess_goddess__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_add_note_add_note__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_add_item_add_item__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_item_details_item_details__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_note_details_note_details__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__providers_data_data__ = __webpack_require__(327);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__providers_datanote_datanote__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_splash_splash__ = __webpack_require__(325);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24_ng2_dragula__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24_ng2_dragula___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_24_ng2_dragula__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













//import { DatepickerPage } from '../pages/datepicker/datepicker';
//import { HabittrackerPage } from './../pages/habittracker/habittracker';












//import { DatanoteProvider} from '../providers/datanote/datanote';
//import { NotedataProvider } from '../providers/notedata/notedata';
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_6__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_11__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_12__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_todo_todo__["a" /* TodoPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_note_note__["a" /* NotePage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_add_note_add_note__["a" /* AddNotePage */],
            __WEBPACK_IMPORTED_MODULE_18__pages_add_item_add_item__["a" /* AddItemPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_item_details_item_details__["a" /* ItemDetailsPage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_note_details_note_details__["a" /* NoteDetailsPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_goddess_goddess__["a" /* GoddessPage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_splash_splash__["a" /* Splash */],
            __WEBPACK_IMPORTED_MODULE_4__pages_tab_tab__["a" /* TabPage */],
            __WEBPACK_IMPORTED_MODULE_3__pages_priorities_t_priorities_t__["a" /* PrioritiesTPage */],
            __WEBPACK_IMPORTED_MODULE_2__pages_todo_t_todo_t__["a" /* TodoTPage */],
            __WEBPACK_IMPORTED_MODULE_1__pages_apmnt_t_apmnt_t__["a" /* ApmntTPage */],
            __WEBPACK_IMPORTED_MODULE_0__pages_grate_t_grate_t__["a" /* GrateTPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_13_ionic2_Calendar__["a" /* NgCalendarModule */],
            __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_7_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_11__app_component__["a" /* MyApp */], {}, {
                links: [
                    { loadChildren: '../pages/priorities-t/priorities-t.module#PrioritiesTPageModule', name: 'PrioritiesTPage', segment: 'priorities-t', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/todo-t/todo-t.module#TodoTPageModule', name: 'TodoTPage', segment: 'todo-t', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/grate-t/grate-t.module#GrateTPageModule', name: 'GrateTPage', segment: 'grate-t', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/apmnt-t/apmnt-t.module#ApmntTPageModule', name: 'ApmntTPage', segment: 'apmnt-t', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/tab/tab.module#TabPageModule', name: 'TabPage', segment: 'tab', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/add-item/add-item.module#AddItemPageModule', name: 'AddItemPage', segment: 'add-item', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/item-details/item-details.module#ItemDetailsPageModule', name: 'ItemDetailsPage', segment: 'item-details', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/add-note/add-note.module#AddNotePageModule', name: 'AddNotePage', segment: 'add-note', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/note-details/note-details.module#NoteDetailsPageModule', name: 'NoteDetailsPage', segment: 'note-details', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/note/note.module#NotePageModule', name: 'NotePage', segment: 'note', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/goddess/goddess.module#GoddessPageModule', name: 'GoddessPage', segment: 'goddess', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/event-modal/event-modal.module#EventModalPageModule', name: 'EventModalPage', segment: 'event-modal', priority: 'low', defaultHistory: [] }
                ]
            }),
            __WEBPACK_IMPORTED_MODULE_8__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_24_ng2_dragula__["DragulaModule"]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_7_ionic_angular__["b" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_11__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_12__pages_home_home__["a" /* HomePage */],
            //HabittrackerPage,
            //DatepickerPage,
            __WEBPACK_IMPORTED_MODULE_14__pages_todo_todo__["a" /* TodoPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_note_note__["a" /* NotePage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_add_note_add_note__["a" /* AddNotePage */],
            __WEBPACK_IMPORTED_MODULE_18__pages_add_item_add_item__["a" /* AddItemPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_item_details_item_details__["a" /* ItemDetailsPage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_note_details_note_details__["a" /* NoteDetailsPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_goddess_goddess__["a" /* GoddessPage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_splash_splash__["a" /* Splash */],
            __WEBPACK_IMPORTED_MODULE_4__pages_tab_tab__["a" /* TabPage */],
            __WEBPACK_IMPORTED_MODULE_3__pages_priorities_t_priorities_t__["a" /* PrioritiesTPage */],
            __WEBPACK_IMPORTED_MODULE_2__pages_todo_t_todo_t__["a" /* TodoTPage */],
            __WEBPACK_IMPORTED_MODULE_1__pages_apmnt_t_apmnt_t__["a" /* ApmntTPage */],
            __WEBPACK_IMPORTED_MODULE_0__pages_grate_t_grate_t__["a" /* GrateTPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__["a" /* SplashScreen */],
            { provide: __WEBPACK_IMPORTED_MODULE_6__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_7_ionic_angular__["c" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_21__providers_data_data__["a" /* Data */],
            __WEBPACK_IMPORTED_MODULE_22__providers_datanote_datanote__["a" /* Datanote */]
            //NotedataProvider
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 390:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 168,
	"./af.js": 168,
	"./ar": 169,
	"./ar-dz": 170,
	"./ar-dz.js": 170,
	"./ar-kw": 171,
	"./ar-kw.js": 171,
	"./ar-ly": 172,
	"./ar-ly.js": 172,
	"./ar-ma": 173,
	"./ar-ma.js": 173,
	"./ar-sa": 174,
	"./ar-sa.js": 174,
	"./ar-tn": 175,
	"./ar-tn.js": 175,
	"./ar.js": 169,
	"./az": 176,
	"./az.js": 176,
	"./be": 177,
	"./be.js": 177,
	"./bg": 178,
	"./bg.js": 178,
	"./bn": 179,
	"./bn.js": 179,
	"./bo": 180,
	"./bo.js": 180,
	"./br": 181,
	"./br.js": 181,
	"./bs": 182,
	"./bs.js": 182,
	"./ca": 183,
	"./ca.js": 183,
	"./cs": 184,
	"./cs.js": 184,
	"./cv": 185,
	"./cv.js": 185,
	"./cy": 186,
	"./cy.js": 186,
	"./da": 187,
	"./da.js": 187,
	"./de": 188,
	"./de-at": 189,
	"./de-at.js": 189,
	"./de-ch": 190,
	"./de-ch.js": 190,
	"./de.js": 188,
	"./dv": 191,
	"./dv.js": 191,
	"./el": 192,
	"./el.js": 192,
	"./en-au": 193,
	"./en-au.js": 193,
	"./en-ca": 194,
	"./en-ca.js": 194,
	"./en-gb": 195,
	"./en-gb.js": 195,
	"./en-ie": 196,
	"./en-ie.js": 196,
	"./en-nz": 197,
	"./en-nz.js": 197,
	"./eo": 198,
	"./eo.js": 198,
	"./es": 199,
	"./es-do": 200,
	"./es-do.js": 200,
	"./es.js": 199,
	"./et": 201,
	"./et.js": 201,
	"./eu": 202,
	"./eu.js": 202,
	"./fa": 203,
	"./fa.js": 203,
	"./fi": 204,
	"./fi.js": 204,
	"./fo": 205,
	"./fo.js": 205,
	"./fr": 206,
	"./fr-ca": 207,
	"./fr-ca.js": 207,
	"./fr-ch": 208,
	"./fr-ch.js": 208,
	"./fr.js": 206,
	"./fy": 209,
	"./fy.js": 209,
	"./gd": 210,
	"./gd.js": 210,
	"./gl": 211,
	"./gl.js": 211,
	"./gom-latn": 212,
	"./gom-latn.js": 212,
	"./he": 213,
	"./he.js": 213,
	"./hi": 214,
	"./hi.js": 214,
	"./hr": 215,
	"./hr.js": 215,
	"./hu": 216,
	"./hu.js": 216,
	"./hy-am": 217,
	"./hy-am.js": 217,
	"./id": 218,
	"./id.js": 218,
	"./is": 219,
	"./is.js": 219,
	"./it": 220,
	"./it.js": 220,
	"./ja": 221,
	"./ja.js": 221,
	"./jv": 222,
	"./jv.js": 222,
	"./ka": 223,
	"./ka.js": 223,
	"./kk": 224,
	"./kk.js": 224,
	"./km": 225,
	"./km.js": 225,
	"./kn": 226,
	"./kn.js": 226,
	"./ko": 227,
	"./ko.js": 227,
	"./ky": 228,
	"./ky.js": 228,
	"./lb": 229,
	"./lb.js": 229,
	"./lo": 230,
	"./lo.js": 230,
	"./lt": 231,
	"./lt.js": 231,
	"./lv": 232,
	"./lv.js": 232,
	"./me": 233,
	"./me.js": 233,
	"./mi": 234,
	"./mi.js": 234,
	"./mk": 235,
	"./mk.js": 235,
	"./ml": 236,
	"./ml.js": 236,
	"./mr": 237,
	"./mr.js": 237,
	"./ms": 238,
	"./ms-my": 239,
	"./ms-my.js": 239,
	"./ms.js": 238,
	"./my": 240,
	"./my.js": 240,
	"./nb": 241,
	"./nb.js": 241,
	"./ne": 242,
	"./ne.js": 242,
	"./nl": 243,
	"./nl-be": 244,
	"./nl-be.js": 244,
	"./nl.js": 243,
	"./nn": 245,
	"./nn.js": 245,
	"./pa-in": 246,
	"./pa-in.js": 246,
	"./pl": 247,
	"./pl.js": 247,
	"./pt": 248,
	"./pt-br": 249,
	"./pt-br.js": 249,
	"./pt.js": 248,
	"./ro": 250,
	"./ro.js": 250,
	"./ru": 251,
	"./ru.js": 251,
	"./sd": 252,
	"./sd.js": 252,
	"./se": 253,
	"./se.js": 253,
	"./si": 254,
	"./si.js": 254,
	"./sk": 255,
	"./sk.js": 255,
	"./sl": 256,
	"./sl.js": 256,
	"./sq": 257,
	"./sq.js": 257,
	"./sr": 258,
	"./sr-cyrl": 259,
	"./sr-cyrl.js": 259,
	"./sr.js": 258,
	"./ss": 260,
	"./ss.js": 260,
	"./sv": 261,
	"./sv.js": 261,
	"./sw": 262,
	"./sw.js": 262,
	"./ta": 263,
	"./ta.js": 263,
	"./te": 264,
	"./te.js": 264,
	"./tet": 265,
	"./tet.js": 265,
	"./th": 266,
	"./th.js": 266,
	"./tl-ph": 267,
	"./tl-ph.js": 267,
	"./tlh": 268,
	"./tlh.js": 268,
	"./tr": 269,
	"./tr.js": 269,
	"./tzl": 270,
	"./tzl.js": 270,
	"./tzm": 271,
	"./tzm-latn": 272,
	"./tzm-latn.js": 272,
	"./tzm.js": 271,
	"./uk": 273,
	"./uk.js": 273,
	"./ur": 274,
	"./ur.js": 274,
	"./uz": 275,
	"./uz-latn": 276,
	"./uz-latn.js": 276,
	"./uz.js": 275,
	"./vi": 277,
	"./vi.js": 277,
	"./x-pseudo": 278,
	"./x-pseudo.js": 278,
	"./yo": 279,
	"./yo.js": 279,
	"./zh-cn": 280,
	"./zh-cn.js": 280,
	"./zh-hk": 281,
	"./zh-hk.js": 281,
	"./zh-tw": 282,
	"./zh-tw.js": 282
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 390;

/***/ }),

/***/ 416:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_home_home__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_tab_tab__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_splash_splash__ = __webpack_require__(325);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_todo_todo__ = __webpack_require__(326);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_note_note__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_goddess_goddess__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__ = __webpack_require__(324);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__ = __webpack_require__(104);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import { DatepickerPage } from './../pages/datepicker/datepicker';







var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, modalCtrl) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_1__pages_tab_tab__["a" /* TabPage */];
        platform.ready().then(function () {
            statusBar.styleDefault();
            var splash = modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__pages_splash_splash__["a" /* Splash */]);
            splash.present();
        });
        this.pages = [
            { title: ' Daily Tracker', component: __WEBPACK_IMPORTED_MODULE_1__pages_tab_tab__["a" /* TabPage */] },
            { title: ' Calender', component: __WEBPACK_IMPORTED_MODULE_0__pages_home_home__["a" /* HomePage */] },
            //{title: 'page oka', component:EventModalPage},
            // {title:' HabitTracker', component:HabittrackerPage},
            //{title:'DatePicker', component:DatepickerPage},
            { title: 'To-Dos', component: __WEBPACK_IMPORTED_MODULE_4__pages_todo_todo__["a" /* TodoPage */] },
            { title: ' Note', component: __WEBPACK_IMPORTED_MODULE_5__pages_note_note__["a" /* NotePage */] },
            { title: ' My Goddess Time', component: __WEBPACK_IMPORTED_MODULE_6__pages_goddess_goddess__["a" /* GoddessPage */] }
        ];
        this.activePage = this.pages[0];
    }
    MyApp.prototype.openPage = function (page) {
        this.nav.setRoot(page.component);
        this.activePage = page;
    };
    MyApp.prototype.checkActive = function (page) {
        return page == this.activePage;
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_7__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["h" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["h" /* Nav */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_7__angular_core__["Component"])({template:/*ion-inline-start:"/Users/deepakbanger/Desktop/gofro/src/app/app.html"*/'<ion-menu [content]="content">\n    <ion-header>\n        <ion-toolbar>\n            <ion-title>\n                <!--  <span id="pro">Productivity</span>\n                <Br>\n                <span id="god"> Goddess </span>-->\n                <img src="../assets/images/Productivity.png" id="logo">\n            </ion-title>\n        </ion-toolbar>\n    </ion-header>\n    <ion-content padding>\n        <ion-list>\n            <button menuClose ion-item *ngFor="let p of pages" [class.activeHighlight]="checkActive(p)" (click)="openPage(p)">  \n                {{p.title}}\n                \n            </button>\n\n        </ion-list>\n\n    </ion-content>\n</ion-menu>\n\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/Users/deepakbanger/Desktop/gofro/src/app/app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["g" /* ModalController */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 421:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TodoTPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the TodoTPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TodoTPage = (function () {
    function TodoTPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.title = this.navParams.get('move');
    }
    TodoTPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TodoTPage');
    };
    return TodoTPage;
}());
TodoTPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-todo-t',template:/*ion-inline-start:"/Users/deepakbanger/Desktop/gofro/src/pages/todo-t/todo-t.html"*/'<ion-content padding>\n    {{title}}\n</ion-content>'/*ion-inline-end:"/Users/deepakbanger/Desktop/gofro/src/pages/todo-t/todo-t.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
], TodoTPage);

//# sourceMappingURL=todo-t.js.map

/***/ })

},[329]);
//# sourceMappingURL=main.js.map